import { useState } from "react";
import Chat from "./components/Chat/Chat";
import LoginPage from "./components/LoginPage/LoginPage";
import UserList from "./components/UserList/UserList";
import UserEditor from "./components/UserEditor/UserEditor";
import MessageEditor from "./components/MessageEditor/MessageEditor";
import { Route } from "react-router";
import { BrowserRouter, Redirect } from "react-router-dom";
import "./App.css";

function App() {
  const [isAdmin, setIsAdmin] = useState(undefined);
  const [editUser, setIsEditUser] = useState(undefined);
  const [editMessage, setEditMessage] = useState(undefined);

  const getLocation = () => {
    if (typeof isAdmin === "boolean" && editMessage) {
      return "/message-editor";
    }
    if (isAdmin) {
      if (editUser) {
        return "/user-editor";
      } else {
        return "/user-list";
      }
    } else if (isAdmin === false) {
      return "/chat";
    } else {
      return "/login";
    }
  };

  return (
    <BrowserRouter>
      <Route
        path="/login"
        render={() => <LoginPage setIsAdmin={setIsAdmin} />}
      />
      <Route
        path="/chat"
        render={() => (
          <Chat
            url="https://edikdolynskyi.github.io/react_sources/messages.json"
            setEditMessage={setEditMessage}
          />
        )}
      />
      <Route
        path="/user-list"
        render={() => <UserList setIsEditUser={setIsEditUser} />}
      />
      <Route
        path="/user-editor"
        render={() => (
          <UserEditor editUser={editUser} setIsEditUser={setIsEditUser} />
        )}
      />
      <Route
        path="/message-editor"
        render={() => (
          <MessageEditor
            setEditMessage={setEditMessage}
            editMessage={editMessage}
          />
        )}
      />
      <Redirect to={`${getLocation()}`} />
    </BrowserRouter>
  );
}

export default App;
