import React, { useEffect } from "react";
import "./Chat.css";
import Header from "./Header/Header";
import MessageInput from "./MessageInput/MessageInput";
import MessageList from "./MessageList/MessageList";
import Preloader from "./Preloader/Preloader";
import { useDispatch } from "react-redux";

import { connect } from "react-redux";
import * as preloaderActions from "../../redux-preloader/actions";
import * as messagesActions from "../../redux-messages/actions";
import * as editModalActions from "../../redux-editModal/actions";
import * as changeMessageIdActions from "../../redux-change-message-id/actions";

const myUserId = "my-id";
let isLoaded = false;

const Chat = (props) => {
  const dispatch = useDispatch();

  (async () => {
    if (!isLoaded) {
      isLoaded = true;
      await props.fetchMessages(dispatch);
      props.hidePreloader();
    }
  })();

  const getCurrentDate = () => {
    const dateMs = new Date();
    const dateStr = dateMs.toISOString();
    return dateStr;
  };

  const onDeleteMessage = (id) => {
    props.deleteMessage(id, dispatch);
  };

  const onLikeMessage = (id) => {
    props.likeMessage(id);
  };

  const onCreateMessage = (text) => {
    props.addMessage(
      {
        avatar:
          "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
        createdAt: getCurrentDate(),
        editedAt: "",
        text,
        user: "Yuliia",
        userId: myUserId,
      },
      dispatch
    );
  };

  const onChangeMessage = (id) => {
    props.setEditMessage(id);
  };

  const handleKeyPress = (event) => {
    if (event.keyCode === 38) {
      const OwnMessages = props.messages.filter(
        (message) => message.userId === myUserId
      );
      if (OwnMessages.length) {
        const lastOwnMessage = OwnMessages[OwnMessages.length - 1];
        onChangeMessage(lastOwnMessage.id);
      }
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", handleKeyPress);
    return function cleanup() {
      document.removeEventListener("keydown", handleKeyPress);
    };
  });

  return (
    <div className="chat">
      {props.preloader ? (
        <Preloader />
      ) : (
        <div className="chat-components-wrapper">
          <Header messages={props.messages.messages} />
          <MessageList
            messages={props.messages.messages}
            myUserId={myUserId}
            onDeleteMessage={onDeleteMessage}
            onLikeMessage={onLikeMessage}
            onChangeMessage={onChangeMessage}
          />
          <MessageInput onCreateMessage={onCreateMessage} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    preloader: state.preloader,
    messages: state.messages,
    editModal: state.editModal,
    changeMessageId: state.changeMessageId,
  };
};

const mapDispatchToProps = {
  ...preloaderActions,
  ...messagesActions,
  ...editModalActions,
  ...changeMessageIdActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
