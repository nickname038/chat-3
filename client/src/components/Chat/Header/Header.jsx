import React from "react";
import "./Header.css";

const Header = (props) => {
  const getStringDate = (time) => String(time).length === 1 ? `0${time}` : time;

    const users = [];
    props.messages.forEach((message) => {
      if (!users.includes(message.user)) {
        users.push(message.user);
      }
    });
    let lastMessage;
    let day, month, year, hours, minutes;

    if (props.messages.length) {
      lastMessage = props.messages[props.messages.length - 1];
      const date = new Date(lastMessage.createdAt);
      day = getStringDate(date.getDate());
      month = getStringDate(date.getMonth() + 1);
      year = date.getFullYear();
      hours = getStringDate(date.getHours());
      minutes = getStringDate(date.getMinutes());
    }

    return (
      <div className="header">
        <div className="header-info-wrapper">
          <h1 className="header-title">My chat</h1>
          <span className="header-users-count">
            {users.length + " "} participants
          </span>
          <span className="header-messages-count">
            {props.messages.length}
          </span>
          <span>messages</span>
        </div>
        <span className="header-last-message-date">
          {lastMessage
            ? `${day}.${month}.${year} ${hours}:${minutes}`
            : ""}
        </span>
      </div>
    );
}

export default Header;
