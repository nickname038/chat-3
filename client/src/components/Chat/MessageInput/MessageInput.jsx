import React from "react";
import "./MessageInput.css";

const MessageInput = (props) => {

  const onClick = (e) => {
    const textarea = document.querySelector("textarea");
    const text = textarea.value;
    textarea.value = "";
    props.onCreateMessage(text);
  }

    return (
      <div className="message-input">
        <form className="message-input-form">
          <textarea
            className="message-input-text"
            placeholder="Message"
          ></textarea>
          <button
            type="button"
            className="message-input-button"
            onClick={onClick}
          >
           Send
          </button>
        </form>
      </div>
    );
}

export default MessageInput;
