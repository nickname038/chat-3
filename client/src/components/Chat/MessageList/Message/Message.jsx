import React from "react";
import "./Message.css";
import MessageMain from "./MessageMain/MessageMain";
import MessageDivider from "../MessageDivider/MessageDivider";

const Message = (props) => {
  const onLike = (e) => {
    props.onLikeMessage(props.message.id);
  };

  return (
    <div className="message-divider-wrapper">
      {props.isNewDayMessage && <MessageDivider date={props.date} />}
      <div
        className={`message ${props.message.isLiked ? " message-liked" : ""}`}
      >
        <img
          className="message-user-avatar"
          src={props.message.avatar}
          alt=""
        />
        <div className="massage-info-wrapper">
          <MessageMain
            text={props.message.text}
            time={props.message.createdAt}
          />
          <div className="message-footer">
            <button className="message-like" onClick={onLike}>
              <span>Like</span>
              <span className="like-icon"> ❤</span>
            </button>
            <span className="message-user-name">{props.message.user}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Message;
