import React from "react";
import "./MessageMain.css";

const MessageMain = (props) => {
  return (
    <div className="message-main">
      <span className="message-time">{props.time.substr(11, 5)}</span>
      <span className="message-text">{props.text}</span>
    </div>
  );
};

export default MessageMain;
