import React from "react";
import "./OwnMessage.css";
import MessageMain from "../MessageMain/MessageMain";
import MessageDivider from "../../MessageDivider/MessageDivider";

const OwnMessage = (props) => {
  const onDelete = () => {
    props.onDeleteMessage(props.message.id);
  };

  const onChange = (e) => {
    props.onChangeMessage(props.message.id);
  };

  return (
    <div className="message-divider-wrapper">
      {props.isNewDayMessage && <MessageDivider date={props.date} />}
      <div className="own-message">
        <MessageMain text={props.message.text} time={props.message.createdAt} />
        <div className="message-footer">
          <button className="message-edit" onClick={onChange}>
            Edit
          </button>
          <button className="message-delete" onClick={onDelete}>
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default OwnMessage;
