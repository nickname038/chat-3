import React from "react";
import "./MessageList.css";
import Message from "./Message/Message";
import OwnMessage from "./Message/OwnMessage/OwnMessage";

let currData;
let newData;
let isNewDayMessage;

const MessageList = (props) => {
  const renderMessages = props.messages.map((message, i) => {
    newData = message.createdAt.substr(0, 10);
    isNewDayMessage = currData !== newData || i === 0;
    currData = newData;
    if (message.userId === props.myUserId) {
      return (
        <OwnMessage
          message={message}
          isNewDayMessage={isNewDayMessage}
          date={currData}
          onChangeMessage={props.onChangeMessage}
          resetChangingFlag={props.resetChangingFlag}
          onDeleteMessage={props.onDeleteMessage}
          key={message.id}
        />
      );
    }
    return (
      <Message
        message={message}
        isNewDayMessage={isNewDayMessage}
        date={currData}
        onLikeMessage={props.onLikeMessage}
        key={message.id}
      />
    );
  });
  return (
    <div className="message-wrapper">
      <div className="message-list">{renderMessages}</div>
    </div>
  );
};

export default MessageList;
