import { useState } from "react";
import "./LoginPage.css";
import { loginAuth } from "../../services/domainRequest/auth";

const LoginPage = (props) => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  const onChangeLogin = (event) => {
    setLogin(event.target.value);
  };

  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    const data = await loginAuth({ login, password });
    if (!data.error) {
      props.setIsAdmin(data.isAdmin);
    }
  };

  return (
    <div className="form-wrapper">
      <form className="login-form" onSubmit={onSubmit}>
        <label>
          <span>Login:</span>
          <input
            type="text"
            name="login"
            value={login}
            onChange={onChangeLogin}
          />
        </label>
        <label>
          <span>Password:</span>
          <input
            type="password"
            name="password"
            value={password}
            onChange={onChangePassword}
          />
        </label>
        <input className="login-button" type="submit" value="Submit" />
      </form>
    </div>
  );
};

export default LoginPage;
