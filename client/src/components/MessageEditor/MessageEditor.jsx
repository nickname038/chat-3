import React from "react";
import "./MessageEditor.css";
import { useState } from "react";
import { connect } from "react-redux";
import { useDispatch } from "react-redux";
import * as messagesActions from "../../redux-messages/actions";

const MessageEditor = (props) => {
  const dispatch = useDispatch();
  const editMessage = props.messages.find(
    (message) => message.id === props.editMessage
  );
  const [messageText, setMessageText] = useState(editMessage?.text || "");

  const onChange = (e) => {
    const value = e.target.value;
    setMessageText(value);
  };

  const onClickOk = () => {
    const text = messageText || props.changeMessage.text;
    if (text) {
      props.updateMessage(editMessage.id, { text }, dispatch);
      props.setEditMessage();
    }
  };

  const onClickCancel = () => {
    props.setEditMessage();
  };

  return (
    <div className="modal-wrapper">
      <h1 className="modal-title">Edit message</h1>
      <textarea
        className="edit-message-input"
        value={messageText || props.changeMessage?.text}
        onChange={(e) => onChange(e)}
      ></textarea>
      <div className="buttons-wrapper">
        <button
          type="button"
          className="edit-message-button"
          onClick={onClickOk}
        >
          OK
        </button>
        <button
          type="button"
          className="edit-message-close"
          onClick={onClickCancel}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    messages: state.messages.messages,
  };
};

const mapDispatchToProps = {
  ...messagesActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);
