import "./UserEditor.css";
import { useState } from "react";
import { connect } from "react-redux";
import * as usersActions from "../../redux-users/actions";
import { useDispatch } from "react-redux";

const UserEditor = (props) => {
  const dispatch = useDispatch();
  const editUser = props.users.find((user) => user.id === props.editUser);
  const [login, setLogin] = useState(editUser?.login || "");
  const [password, setPassword] = useState(editUser?.password || "");

  const onClickOk = () => {
    if (login && password) {
      if (typeof props.editUser === "boolean") {
        props.addUser({ login, password }, dispatch);
      } else {
        props.updateUser(props.editUser, { login, password }, dispatch);
      }
      props.setIsEditUser(null);
    }
  };

  const onClickCancel = () => {
    props.setIsEditUser(null);
  };

  const onLoginChange = (e) => {
    const value = e.target.value;
    setLogin(value);
  };

  const onPasswordChange = (e) => {
    const value = e.target.value;
    setPassword(value);
  };

  return (
    <div className="user-editor">
      <h5 className="editor-title">Add user</h5>
      <div className="editor-body">
        <div className="editor-input">
          <label className="editor-input-label">Login</label>
          <input
            className="editor-input-field"
            value={login}
            onChange={(e) => onLoginChange(e)}
          />
        </div>
        <div className="editor-input">
          <label className="editor-input-label">Password</label>
          <div className="password-input">
            <input
              className="editor-input-field"
              value={password}
              onChange={(e) => onPasswordChange(e)}
              type={"text"}
            />
          </div>
        </div>
      </div>
      <div className="editor-footer">
        <button className="cancel-button" onClick={onClickCancel}>
          Cancel
        </button>
        <button className="save-button" onClick={onClickOk}>
          Save
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    users: state.users.users,
  };
};

const mapDispatchToProps = {
  ...usersActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserEditor);
