import "./UserItem.css";

const UserItem = (props) => {
  const { id, login } = props;
  const onDelete = () => {
    props.onDelete(id);
  };

  const onEdit = () => {
    props.onEdit(id);
  };

  return (
    <div className="user-item">
      <span className="login">{login}</span>
      <div className="user-control">
        <button onClick={onEdit}> Edit</button>
        <button onClick={onDelete}>Delete</button>
      </div>
    </div>
  );
};

export default UserItem;
