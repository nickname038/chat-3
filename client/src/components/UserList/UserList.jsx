import UserItem from "./UserItem/UserItem";
import "./UserList.css";
import { connect } from "react-redux";
import * as usersActions from "../../redux-users/actions";
import * as preloaderActions from "../../redux-preloader/actions";
import { useDispatch } from "react-redux";
let isLoaded = false;

const UserList = (props) => {
  const dispatch = useDispatch();

  (async () => {
    if (!isLoaded) {
      isLoaded = true;
      await props.fetchUsers(
        dispatch,
        props.showPreloader,
        props.hidePreloader
      );
      props.hidePreloader();
    }
  })();

  const onEdit = (id) => {
    props.setIsEditUser(id);
  };

  const onDelete = (id) => {
    props.deleteUser(id, dispatch);
  };

  const onAdd = () => {
    props.setIsEditUser(true);
  };

  return (
    <div className="user-list">
      <button className="add-user-button" onClick={onAdd}>
        Add user
      </button>
      <div className="user-items">
        {props.users.map((user) => {
          return (
            <UserItem
              key={user.id}
              id={user.id}
              login={user.login}
              password={user.password}
              onDelete={onDelete}
              onEdit={onEdit}
            />
          );
        })}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    preloader: state.preloader,
    messages: state.messages,
    editModal: state.editModal,
    changeMessageId: state.changeMessageId,
    users: state.users.users,
    state: state,
  };
};

const mapDispatchToProps = {
  ...preloaderActions,
  ...usersActions,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
