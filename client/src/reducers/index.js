import { combineReducers } from "redux";
import preloader from "../redux-preloader/reducer";
import messages from "../redux-messages/reducer";
import editModal from "../redux-editModal/reducer";
import changeMessageId from "../redux-change-message-id/reducer";
import users from "../redux-users/reducer"

const rootReducer = combineReducers({
    preloader,
    messages,
    editModal,
    changeMessageId,
    users
});

export default rootReducer;
