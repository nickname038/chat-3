import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  REQUESTED_MESSAGES,
} from "./actionTypes";
import {
  getMessages,
  updateMessage as updatedMessageRequest,
  deleteMessage as deleteMessageRequest,
  createMessage,
} from "../services/domainRequest/messageRequest";

export const fetchMessages = (dispatch) => {
  getMessages().then(
    (data) => {
      dispatch(requestMessagesSuccess(REQUESTED_MESSAGES, data));
    },
    (err) => dispatch(requestMessagesError())
  );
  return { type: "" };
};

const requestMessagesError = () => ({
  type: REQUESTED_MESSAGES,
  payload: { error: true },
});

const requestMessagesSuccess = (type, data) => {
  return {
    type,
    payload: {
      data,
    },
  };
};

export const addMessage = (data, dispatch) => {
  createMessage(data).then(
    (data) => {
      dispatch(requestMessagesSuccess(ADD_MESSAGE, data));
    },
    (err) => dispatch(requestMessagesError())
  );
  return {
    type: "",
  };
};

export const updateMessage = (id, data, dispatch) => {
  updatedMessageRequest(id, data).then(
    (data) => {
      dispatch(requestMessagesSuccess(UPDATE_MESSAGE, data));
    },
    (err) => dispatch(requestMessagesError())
  );
  return {
    type: "",
  };
};

export const deleteMessage = (id, dispatch) => {
  deleteMessageRequest(id).then(
    (data) => {
      dispatch(requestMessagesSuccess(DELETE_MESSAGE, data));
    },
    (err) => dispatch(requestMessagesError())
  );
  return {
    type: "",
  };
};

export const likeMessage = (id) => ({
  type: LIKE_MESSAGE,
  payload: {
    id,
  },
});
