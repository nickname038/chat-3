import {
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  REQUESTED_MESSAGES,
} from "./actionTypes";

const initialState = { messages: [], error: false };

export default function messageReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      const newMessage = { id, ...data, isLiked: false };
      return { messages: [...state.messages, newMessage], error: false };
    }

    case DELETE_MESSAGE: {
      const { data } = action.payload;
      const filteredMessages = state.messages.filter(
        (message) => message.id !== data.id
      );
      return { messages: filteredMessages, error: false };
    }

    case LIKE_MESSAGE: {
      const { id } = action.payload;
      const likedMessage = state.messages.find((message) => message.id === id);
      likedMessage.isLiked = !likedMessage.isLiked;
      return { messages: [...state.messages], error: false };
    }

    case UPDATE_MESSAGE: {
      const { data } = action.payload;
      const editedMessageOld = state.messages.find(
        (message) => message.id === data.id
      );
      const indexOfEditedMessageOld = state.messages.indexOf(editedMessageOld);
      const editedMessageNew = {
        ...editedMessageOld,
        text: data.text,
        editedAt: data.editedAt,
      };
      state.messages.splice(indexOfEditedMessageOld, 1, editedMessageNew);
      return { messages: [...state.messages], error: false };
    }
    case REQUESTED_MESSAGES: {
      const { data, error } = action.payload;
      if (error) {
        return { error: true, ...state };
      } else {
        return { error: false, messages: data };
      }
    }

    default:
      return state;
  }
}
