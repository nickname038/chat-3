import {
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  REQUESTED_USERS,
} from "./actionTypes";
import {
  getUsers,
  updateUser as updatedUserRequest,
  deleteUser as deleteUserRequest,
  createUser,
} from "../services/domainRequest/userRequest";

export const fetchUsers = (dispatch) => {
  getUsers().then(
    (data) => {
      dispatch(requestUsersSuccess(REQUESTED_USERS, data));
    },
    (err) => dispatch(requestUsersError())
  );
  return { type: "" };
};

const requestUsersError = () => ({
  type: REQUESTED_USERS,
  payload: { error: true },
});

const requestUsersSuccess = (type, data) => {
  return {
    type,
    payload: {
      data,
    },
  };
};

export const addUser = (data, dispatch) => {
  createUser(data).then(
    (data) => {
      dispatch(requestUsersSuccess(ADD_USER, data));
    },
    (err) => dispatch(requestUsersError())
  );
  return {
    type: "",
  };
};

export const updateUser = (id, data, dispatch) => {
  updatedUserRequest(id, data).then(
    (data) => {
      dispatch(requestUsersSuccess(UPDATE_USER, data));
    },
    (err) => dispatch(requestUsersError())
  );
  return {
    type: "",
  };
};

export const deleteUser = (id, dispatch) => {
  deleteUserRequest(id).then(
    (data) => {
      dispatch(requestUsersSuccess(DELETE_USER, data));
    },
    (err) => dispatch(requestUsersError())
  );
  return {
    type: "",
  };
};
