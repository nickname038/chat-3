import {
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
  REQUESTED_USERS,
} from "./actionTypes";

const initialState = {
  users: [],
  error: false,
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_USER: {
      const { id, data } = action.payload;
      const newUser = { id, ...data };
      return { users: [...state.users, newUser], error: false };
    }

    case UPDATE_USER: {
      const { data } = action.payload;
      const updatedUsers = state.users.map((user) => {
        if (user.id === data.id) {
          return {
            ...user,
            ...data,
          };
        } else {
          return user;
        }
      });

      return { users: updatedUsers, error: false };
    }

    case DELETE_USER: {
      const { data } = action.payload;
      const filteredUsers = state.users.filter((user) => user.id !== data.id);
      return { users: filteredUsers, error: false };
    }

    case REQUESTED_USERS: {
      const { data, error } = action.payload;
      if (error) {
        return { error: true, ...state };
      } else {
        return { error: false, users: data };
      }
    }

    default:
      return state;
  }
}

export default userReducer;
