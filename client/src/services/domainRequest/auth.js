import { post } from "../requestHelper";

export const loginAuth = async (body) => {
  return await post("auth/login", body);
};
