import { get, post, put, deleteReq } from "../requestHelper";
const entity = "messages";

export const createMessage = async (body) => {
  return await post(entity, body);
};

export const getMessages = async () => {
  return await get(entity);
};

export const deleteMessage = async (id) => {
  return await deleteReq(entity, id);
};

export const updateMessage = async (id, body) => {
  return await put(entity, id, body);
};
