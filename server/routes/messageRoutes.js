const { Router } = require("express");
const MessageService = require("../services/messageService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    try {
      res.data = MessageService.searchAll();
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params["id"];
      res.data = MessageService.search({ id: id });
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  (req, res, next) => {
    if (!res.err) {
      const message = req.body;
      try {
        res.data = MessageService.create(message);
      } catch (err) {
        res.err = err;
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  (req, res, next) => {
    if (!res.err) {
      try {
        const id = req.params["id"];
        const datatoUpdate = req.body;
        res.data = MessageService.update(id, datatoUpdate);
      } catch (err) {
        res.err = err;
      }
    }
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params["id"];
      res.data = MessageService.delete(id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
