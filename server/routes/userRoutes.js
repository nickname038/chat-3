const { Router } = require("express");
const UserService = require("../services/userService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    try {
      res.data = UserService.searchAll();
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params["id"];
      res.data = UserService.search({ id: id });
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  "/",
  (req, res, next) => {
    if (!res.err) {
      const user = req.body;
      try {
        res.data = UserService.create(user);
      } catch (err) {
        res.err = err;
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  (req, res, next) => {
    if (!res.err) {
      try {
        const id = req.params["id"];
        const datatoUpdate = req.body;
        res.data = UserService.update(id, datatoUpdate);
      } catch (err) {
        res.err = err;
      }
    }
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    try {
      const id = req.params["id"];
      res.data = UserService.delete(id);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
