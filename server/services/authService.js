const UserService = require("./userService");

class AuthService {
  login(userData) {
    const user = UserService.search(userData);
    if (!user) {
      throw Error("User not found");
    } else if (user.login === "admin" && user.password === "admin") {
      return { isAdmin: true };
    } else {
      return { isAdmin: false };
    }
  }
}

module.exports = new AuthService();
