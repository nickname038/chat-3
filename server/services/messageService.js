const { MessageRepository } = require("../repositories/messageRepository");

class MessageService {
  searchAll() {
    const items = MessageRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  search(search) {
    const item = MessageRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const item = MessageRepository.create(data);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, dataToUpdate) {
    if (!this.search({ id: id })) return null;

    const item = MessageRepository.update(id, dataToUpdate);

    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    const items = MessageRepository.delete(id);
    if (!items.length) {
      return null;
    }
    return items[0];
  }
}

module.exports = new MessageService();
