const { UserRepository } = require("../repositories/userRepository");

class UserService {
  searchAll() {
    const items = UserRepository.getAll();
    if (!items) {
      return null;
    }
    return items;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(data) {
    const item = UserRepository.create(data);
    if (!item) {
      return null;
    }
    return item;
  }

  update(id, dataToUpdate) {
    if (!this.search({ id: id })) return null;

    const item = UserRepository.update(id, dataToUpdate);

    if (!item) {
      return null;
    }
    return item;
  }

  delete(id) {
    const items = UserRepository.delete(id);
    if (!items.length) {
      return null;
    }
    return items[0];
  }
}

module.exports = new UserService();
